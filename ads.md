# Creating Ad Posts

Ad posts are created by including data in the post's JSON metadata, according to the requirements of each ad type. More ad types are planned.

# Supported Ad Types on Native Ads

### post_global

This is the native post type found on Hive.

Suitable for use as featured posts within front-ends and individual communities (where the community owner is the Monetizer Account)

#### JSON Metadata

```
{
    "app": "...",
    "native-ads": {
        "type": "post_global"
}
```

For reference, study this post: https://hiveblocks.com/test/@imwatsi.test/cool-post-i-wanna-promote-5
