## Supported Properties for Monetizer Accounts

*name: type (default)*

- `enabled`: `boolean` (false) > *accept ads or not on account*
- `token`: `string` ("@@000000021") > *accepted token for ad payments*
- `ad_types`: `array` (["post_global", "post_tags"]) > *supported ad types*
- `burn`: `boolean` (false) > *whether or not payments must be burned*
- `min_bid`: `floating point number` (10.000) > *minimum amount of tokens for bids*
- `min_time_bid`: `integer number` > *minimum number of time units per bid*
- `max_time_bid`: `integer number` > *maximum number of time units per bid*
- `max_time_active`: `integer number` > *maximum number of active time units per account*
- `scheduled_ads_delay`: `integer number` (1440) > *notice period for scheduled ads in time units*
- `scheduled_ads_timeout`: `integer number` > *adds timeout for scheduled ad payments*

## Monetizer Space Properties

- `ad_type`: `string`
- `title`: `string`
- `description`: `string`
- `guidelines`: `string`