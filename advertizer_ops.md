# Advertizer Ops

All Native Ads ops should be broadcast with a `json` value of an `array` containing three elements:

- acc type (string)
- op name (string)
- op data (object)

`["acc_type", "op_name", {op_data}]`

## account_init

```
{
    "id": "native-ads",
    "json": [
        "advertizer",
        "account_init",
        {
            "profile": {}
        }
    ]
}
```

## ad_submit

The `ad_submit` op is used to make an initial submission to a Monetizer Account. The `start_time` value is optional, for scheduled ads.

#### Valid keys

- `monetizer`: `string` > *Hive account name of Monetizer*
- `ad_space`: `string` > *permlink of ad_space*
- `permlink`: `string` > *permlink of ad_post containing the ad*
- `start_time`: `timestamp (YYYY-MM-DDTHH:MM:SS)` [optional] > *when ad should be scheduled to show*
- `time_units`: `integer` > *minutes to show ad for*
- `bid_amount`: `float` > *how much currency to bid*
- `bid_token`: `string` > *NAI of the currency to bid*

#### Example op

```
{
    "id": "native-ads",
    "json": [
        "advertizer",
        "ad_submit",
        {
            "monetizer": "threespeak",
            "ad_space": "featured-post-of-the-day",
            "permlink": "my-promo-post",
            "time_units": 360,
            "bid_amount": 300,
            "bid_token": "@@000000021"
        }
    ]
}
```

## ad_bid

The `ad_bid` op is used to change an ad's bid within a particular Monetizer's space.

#### Valid keys

- `monetizer`: `string` > *Hive account name of Monetizer*
- `ad_space`: `string` > *permlink of ad_space*
- `permlink`: `string` > *permlink of ad_post containing the ad*
- `start_time`: `timestamp (YYYY-MM-DDTHH:MM:SS)` [optional] > *when ad should be scheduled to show*
- `time_units`: `integer` [optional] > *minutes to show ad for*
- `bid_amount`: `float` > *how much currency to bid*
- `bid_token`: `string` > *NAI of the currency to bid*

#### Example op

```
{
    "id": "native-ads",
    "json": [
        "advertizer",
        "ad_bid",
        {
            "monetizer": "threespeak",
            "ad_space": "featured-post-of-the-day",
            "permlink": "my-promo-post",
            "time_units": 300,
            "bid_amount": 600,
            "bid_token": "@@000000021"
        }
    ]
}
```

# ad_withdraw

The `ad_withdraw` op is used to withdraw an ad from a Monetizer Ad Space, which sends it back to draft status for that space.

#### Example op

```
{
    "id": "native-ads",
    "json": [
        "advertizer",
        "ad_withdraw",
        {
            "monetizer": "hive-133333",
            "ad_space": "featured-post-of-the-day",
            "permlink": "cool-post-i-wanna-promote-3"
        }
    ]
}
```


## ad_fund

The ad fund op is triggered by making a token transfer to the Monetizer Account, with the memo following this scheme:

`hna:{monetizer}/{ad_space}/{ad_permlink}`

For example:

`hna:threespeak/front-page/my-cool-banner-image`

or

`hna:hive-133333/featured-post-of-the-day/cool-post-i-wanna-promote-5`

The sending account must be the owner of the ad post being paid for. Burn payments (for Monetizers who specify this requirement in their properties) are also supported. These are sent to `@null` with the above `memo` scheme.

